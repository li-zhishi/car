# """iotserv URL Configuration

# The `urlpatterns` list routes URLs to views. For more information please see:
#     https://docs.djangoproject.com/en/2.1/topics/http/urls/
# Examples:
# Function views
#     1. Add an import:  from my_app import views
#     2. Add a URL to urlpatterns:  path('', views.home, name='home')
# Class-based views
#     1. Add an import:  from other_app.views import Home
#     2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
# Including another URLconf
#     1. Import the include() function: from django.urls import include, path
#     2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
# """
from django.contrib import admin
from django.urls import path
from django.conf.urls import include,url
from . import views
app_name='car_app'

urlpatterns = [
    path(r'',views.logins,name='logins'),

    path(r'index/',views.index,name='index'),

    path(r'normal_user_index/',views.normal_user_index,name='normal_user_index'),

    path(r'dynamic/',views.dynamic,name='dynamic'),

    path(r'device/',views.device,name='device'),

    path(r'car_manage/',views.car_manage,name='car_manage'),
    path(r'car_apply/',views.car_apply,name='car_apply'),
    path(r'car_apply_approve/',views.car_apply_approve,name='car_apply_approve'),
    path(r'car_board/',views.car_board,name='car_board'),

    path(r'person_manage/',views.person_manage,name='person_manage'),

    path(r'trail/',views.trail,name='trail'),

    path(r'efence/',views.efence,name='efence'),

    path(r'polyline/',views.polyline,name='polyline'),

    path(r'device_report/',views.device_report,name='device_report'),

    path(r'mileage_report/',views.mileage_report,name='mileage_report'),

    path(r'alarm_report/',views.alarm_report,name='alarm_report'),

    path(r'user_manage/',views.user_manage,name='user_manage'),

    path(r'sys_log_web/',views.sys_log_web,name='sys_log_web'),

    # path(r'pc_register/',views.pc_register,name='pc_register'),
    # path(r'forgot_pwd/',views.forgot_pwd,name='forgot_pwd'),

    # 手机端api，第三方api
    path(r'login_api/', views.LoginView.as_view()),  # 登录
    path(r'index_api/', views.BaseInfo.as_view()),  # 获取高频使用基本信息
    path(r'dynamic_api/', views.DynamicApi.as_view()),
    path(r'query_api/', views.QueryApi.as_view()),
    path(r'efence_api/', views.EfenceApi.as_view()),
    path(r'car_apply_api/', views.CarApplyApi.as_view()),
    path(r'car_approve_api/', views.CarApproveApi.as_view()),
    
    # path(r'chat_api/', views.ChatAPIView.as_view()),# 微聊api，下一步完善websocket，与设备实时交互功能
    # path(r'register/',views.RegisteredView.as_view()), # 注册，绑定设备retrieve_password
    # path(r'retrieve_password/',views.Retrieve_passwordView.as_view()),# 重置密码
    # path(r'command/',views.CommandView.as_view()),# 命令处理

]
